package fr.uvsq.uvsq21400579;

import org.junit.Test;

import static org.junit.Assert.*;

public class FractionTest {
    @Test
    public void testZero() {
        System.out.println("TestZero");
    }

    @Test
    public void testDenDiffZero() {
        Fraction f = new Fraction(15, 0);
        assertTrue(f.den != 0);

        Fraction f2 = new Fraction();
        assertTrue(f2.den != 0);

        Fraction f3 = new Fraction(-5);
        assertTrue(f3.den != 0);
    }

    @Test
    public void testGetters() {
        Fraction f = new Fraction(1, 2);
        assertTrue(f.getDen() == 2);
        assertTrue(f.getNum() == 1);
    }

    @Test
    public void testUN(){
        System.out.println("TestUN");

    }
}
