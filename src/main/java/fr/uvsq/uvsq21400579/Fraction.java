package fr.uvsq.uvsq21400579;

public final class Fraction
{
    public final int num;
    public final int den;
    public final int[] ZERO ={0,1};
    public final int[] UN={1,1};

    public Fraction() {
        num = 0;
        den = 1;
    }

    public Fraction(int NUM) {
        num = NUM;
        den = 1;
    }

    public Fraction(int NUM, int DEN) {
        if (DEN == 0) {
            System.out.println("DEN ne peut pas etre null => DEN = 1");
            num = NUM;
            den = 1;
        } else {
            num = NUM;
            den = DEN;
        }
    }

    public int getNum(){
        return num;
    }

    public int getDen() {
            return den;
        }
    public double getNbrDouble() {
                double result = 0;
                if (this.den != 0) {
                    result = this.num / this.den;
                }
                return result;
            }

    public String toString(){
        String s = new String();
        s += this.num+" / "+this.den;
        return s;
    }
}
